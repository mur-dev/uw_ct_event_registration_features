<?php
/**
 * @file
 * uw_ct_event_registration_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_event_registration_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info_alter().
 */
function uw_ct_event_registration_features_node_info_alter(&$data) {
  if (isset($data['uw_ct_event_registration'])) {
    $data['uw_ct_event_registration']['has_title'] = 1; /* WAS: '' */
    $data['uw_ct_event_registration']['help'] = ''; /* WAS: '' */
    unset($data['uw_ct_event_registration']['locked']);
  }
}
