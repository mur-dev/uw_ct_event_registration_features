<?php
/**
 * @file
 * uw_ct_event_registration_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_event_registration_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_event_registration content'.
  $permissions['create uw_ct_event_registration content'] = array(
    'name' => 'create uw_ct_event_registration content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_event_registration content'.
  $permissions['delete any uw_ct_event_registration content'] = array(
    'name' => 'delete any uw_ct_event_registration content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_event_registration content'.
  $permissions['delete own uw_ct_event_registration content'] = array(
    'name' => 'delete own uw_ct_event_registration content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_ct_event_registration content'.
  $permissions['edit any uw_ct_event_registration content'] = array(
    'name' => 'edit any uw_ct_event_registration content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_event_registration content'.
  $permissions['edit own uw_ct_event_registration content'] = array(
    'name' => 'edit own uw_ct_event_registration content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
