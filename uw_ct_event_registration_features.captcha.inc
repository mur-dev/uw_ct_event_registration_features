<?php
/**
 * @file
 * uw_ct_event_registration_features.captcha.inc
 */

/**
 * Implements hook_captcha_default_points().
 */
function uw_ct_event_registration_features_captcha_default_points() {
  $export = array();

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'uw_ct_event_registration_node_form';
  $captcha->module = 'recaptcha';
  $captcha->captcha_type = 'reCAPTCHA';
  $export['uw_ct_event_registration_node_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'uw_ft_event_registration_form';
  $captcha->module = 'recaptcha';
  $captcha->captcha_type = 'reCAPTCHA';
  $export['uw_ft_event_registration_form'] = $captcha;

  return $export;
}
