<?php
/**
 * @file
 * uw_ct_event_registration_features.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_event_registration_features_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: node
  $overrides["node.uw_ct_event_registration.has_title"] = 1;
  $overrides["node.uw_ct_event_registration.help"] = '';
  $overrides["node.uw_ct_event_registration.locked"]["DELETED"] = TRUE;

 return $overrides;
}
